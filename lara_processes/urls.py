"""_____________________________________________________________________

:PROJECT: LARA

*lara_processes urls *

:details: lara_processes urls module.
         - add app specific urls here

:file:    views.py
:authors: mark doerr

:date: (creation)          20180627
:date: (last modification) 20190125

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.2"

from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic import TemplateView
from . import views

# Add your lara_processes urls here.

#~ urlpatterns = [
    #~ # path('', TemplateView.as_view(template_name='index.html'), name='lara_processes-main-view'),
    #~ # path('subdir/', include('.urls')),
#~ ]


app_name = 'lara_processes' # !! this sets the apps namespace to be used in the template

# the 'name' attribute is used in templates to address the url independant of the view
# it is used/called by the {% url %} template tag
urlpatterns = [
    path('', views.index, name='index'),   
    #~ path(r'^processlist$', views.processList, name='process_list'),
    #~ path(r'^(?P<process_id>[0-9]+)/$', views.processDetails, name='process_details'),
    path('search/', views.searchForm, name='searchForm'),
    path('search-results/', views.search, name='search'),
    path('results/', views.results, name='results'),
    path('generator/', 
        TemplateView.as_view(template_name='process_generator/index.html'),
        name='process-generator-index'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

