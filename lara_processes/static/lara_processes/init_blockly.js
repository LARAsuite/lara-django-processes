// initiallise the blockly environment

function initBlockly() {
    
    var blocklyArea = document.getElementById('blocklyArea');   
    var toolbox = document.getElementById("toolbox");
    var blocklyDiv = document.getElementById('blocklyDiv');
        
    var options = { 
        toolbox : toolbox, 
        collapse : true, 
        comments : true, 
        css : true, 
        disable : true, 
        maxBlocks : Infinity, 
        media : 'https://blockly-demo.appspot.com/static/media/', 
        readOnly : false, 
        rtl : false, 
        scrollbars : true, 
        sounds : true, 
        trashcan : true, 
        grid : {
            spacing : 0, 
            length : 1, 
            colour : '#888', 
            snap : false
        }, 
        zoom : {
            controls : true, 
            wheel : true, 
            startScale : 1.2, 
            maxcale : 3, 
            minScale : 0.3
        }
    };
    
    //alert("v0.3: injecting blockly .... ");
            
    //  var workspace = Blockly.inject(blocklyDiv, options );
    var workspace = Blockly.inject('blocklyDiv', options );
    
    //alert("blockly injected");    
    
    /* Load Workspace Blocks from XML to workspace. Remove all code below if no blocks to load */
    /* TODO: Change workspace blocks XML ID if necessary. Can export workspace blocks XML from Workspace Factory. */
    var workspaceBlocks = document.getElementById("workspaceBlocks"); 
    
    /* Load blocks to workspace. */
    Blockly.Xml.domToWorkspace(workspace, workspaceBlocks);

    var onresize = function(e) {
        // Compute the absolute coordinates and dimensions of blocklyArea.
        var element = blocklyArea;
        var x = 0;
        var y = 0;
        do {
          x += element.offsetLeft;
          y += element.offsetTop;
          element = element.offsetParent;
        } while (element);
        // Position blocklyDiv over blocklyArea.
        blocklyDiv.style.left = x + 'px';
        blocklyDiv.style.top = y + 'px';
        blocklyDiv.style.width = blocklyArea.offsetWidth + 'px';
        blocklyDiv.style.height = blocklyArea.offsetHeight + 'px';
    };
  
   // window.addEventListener('resize', onresize, false);
   // onresize();
   // Blockly.svgResize(workspace);
         
    // Let the top-level application know that Blockly is ready.
    window.parent.blockly_loaded(Blockly);
  };
