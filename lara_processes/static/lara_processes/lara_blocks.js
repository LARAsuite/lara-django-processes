Blockly.Blocks['cultivation_growth'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("growth")
        .appendField(new Blockly.FieldImage("http://localhost/media/blockly/growth_120x80.png", 80, 80, "growth"));
    this.appendDummyInput()
        .appendField("temperature  ")
        .appendField(new Blockly.FieldNumber(37, 4, 50), "temp")
        .appendField("°C");
    this.appendDummyInput()
        .appendField("incubation dur.")
        .appendField(new Blockly.FieldNumber(100, 0, 6000), "incubation_duration")
        .appendField("min");
    this.appendDummyInput()
        .appendField("# of cycles      ")
        .appendField(new Blockly.FieldNumber(3, 1, 999), "cycle_num")
        .appendField("x");
    this.setPreviousStatement(true, "process_step");
    this.setNextStatement(true, "process_step");
    this.setColour(0);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['cultivation_expression'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("expression")
        .appendField(new Blockly.FieldImage("http://localhost/media/blockly/expression_120x80.png", 80, 80, "expression"));
    this.appendDummyInput()
        .appendField("temperature  ")
        .appendField(new Blockly.FieldNumber(20, 4, 50), "temp")
        .appendField("°C");
    this.appendDummyInput()
        .appendField("incubation dur.")
        .appendField(new Blockly.FieldNumber(120, 0, 6000), "incubation_duration")
        .appendField("min");
    this.appendDummyInput()
        .appendField("# of cycles      ")
        .appendField(new Blockly.FieldNumber(4, 1, 999), "cycle_num")
        .appendField("x");
    this.setPreviousStatement(true, "process_step");
    this.setNextStatement(true, "process_step");
    this.setColour(20);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['cultivation_harvest'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("harvest")
        .appendField(new Blockly.FieldImage("http://localhost/media/blockly/harvest_cells_120x80.png", 80, 80, "harvest"));
    this.appendDummyInput()
        .appendField("temperature  ")
        .appendField(new Blockly.FieldNumber(4, 4, 50), "temp")
        .appendField("°C");
    this.appendDummyInput()
        .appendField("centrifugation dur.")
        .appendField(new Blockly.FieldNumber(20, 0, 6000), "incubation_duration")
        .appendField("min");
    this.setPreviousStatement(true, "process_step");
    this.setNextStatement(true, "process_step");
    this.setColour(60);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['cultivation_wash'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("wash")
        .appendField(new Blockly.FieldImage("http://localhost/media/blockly/wash_cells_120x80.png", 80, 80, "wash"));
    this.appendDummyInput()
        .appendField("temperature  ")
        .appendField(new Blockly.FieldNumber(37, 4, 50), "temp")
        .appendField("°C");
    this.appendDummyInput()
        .appendField("centrifugation dur.")
        .appendField(new Blockly.FieldNumber(20, 0, 6000), "incubation_duration")
        .appendField("min");
    this.setPreviousStatement(true, "process_step");
    this.setNextStatement(true, "process_step");
    this.setColour(240);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['cultivation_lysis'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("lysis")
        .appendField(new Blockly.FieldImage("http://localhost/media/blockly/lysis_cells_120x80.png", 80, 80, "wash"));
    this.appendDummyInput()
        .appendField("temperature  ")
        .appendField(new Blockly.FieldNumber(30, 4, 50), "temp")
        .appendField("°C");
    this.appendDummyInput()
        .appendField("duration")
        .appendField(new Blockly.FieldNumber(120, 1, 6000), "lysis_duration")
        .appendField("min");
    this.appendDummyInput()
        .appendField("centrifugation dur.")
        .appendField(new Blockly.FieldNumber(20, 0, 6000), "incubation_duration")
        .appendField("min");
    this.setPreviousStatement(true, "process_step");
    this.setNextStatement(true, "process_step");
    this.setColour(285);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['processflow_read_barcode'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("read barcode")
        .appendField(new Blockly.FieldImage("http://localhost/media/blockly/read_barcode_120x80.png", 80, 80, "read_barcode"));
    this.setPreviousStatement(true, "process_step");
    this.setNextStatement(true, "process_step");
    this.setColour(45);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['processflow_init_process'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("init process")
        .appendField(new Blockly.FieldImage("http://localhost/media/blockly/process_begin_120x80.png", 80, 80, "init process"));
    this.setNextStatement(true, null);
    this.setColour(105);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['processflow_end_process'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("end process")
        .appendField(new Blockly.FieldImage("http://localhost/media/blockly/process_end_120x80.png", 80, 80, "init process"));
    this.setPreviousStatement(true, null);
    this.setColour(0);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};
