"""_____________________________________________________________________

:PROJECT: LARA

*lara_processes tests *

:details: lara_processes application tests.
         - 

:file:    tests.py
:authors: mark doerr

:date: (creation)          20180627
:date: (last modification) 20180627

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.1"

from django.test import TestCase
import logging

logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
#~ logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.ERROR)

# Create your tests here. s. https://docs.djangoproject.com/en/1.9/intro/tutorial05/

import datetime

from django.utils import timezone
from django.test import TestCase

from django.core.urlresolvers import reverse # needs to be changed in dango 1.10 to:
#~ from django.urls import reverse

from ..models import Procedure, Process
from lara_metainfo.models import Item_class, MetaInfo

def createProcess(num_items=1):
    """ Creates a list of process entries
    """
    item_class_a = Item_class.objects.create(item_class="process_test_class")

    process_list = [ Process.objects.create(name="process%s" %i, proc_class=item_class_a, proc="process %s" %i, parameters="para1", description="process description %s" %i  ) for i in range(num_items) ]
    
    return process_list
    
def createProcedure(num_items=1):
    """ Creates a list of procedure entries
    """
    item_class_a = Item_class.objects.create(item_class="process_test_class")

    procedure_list = [ Procedure.objects.create(name="procedure%s" %i, proc_class=item_class_a, proc="procedure %s" %i, parameters="para1", description="procedure description %s" %i  ) for i in range(num_items) ]
    
    return procedure_list

class ProcessMethodsTests(TestCase):
    def testProcess(self):
        curr_data = createProcess(num_items=10)
        
class ProcedureMethodsTests(TestCase):
    def testProcedure(self):
        curr_data = createProcedure(num_items=10)
        
class ProcessViewsTests(TestCase):
    def testProcessView(self):
        """ Testing data view
        """
        curr_data = createProcess()
        
        response = self.client.get(reverse('lara_processes:index'))
        
        #~ self.assertContains(response, "test")
        
    def testSearchFormView(self):
        """ Testing search  view
        """
        curr_data = createProcess()
        
        response = self.client.get(reverse('lara_processes:searchForm'))
        
        self.assertContains(response, "Search")
