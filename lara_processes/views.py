"""_____________________________________________________________________

:PROJECT: LARA

*lara_processes views *

:details: lara_processes views module.
         - 

:file:    views.py
:authors: mark doerr

:date: (creation)          20180627
:date: (last modification) 20180627

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.1"

import sys

from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, HttpResponse

from django.urls import reverse

from .models import Procedure, Process

def index(request):
    process_list = Process.objects.all()
    context = {'process_list': process_list}
    return render(request, 'lara_processes/index.html', context)

def processList(request):
    process_list = Process.objects.all()
    table_caption = "List of all LARA data"
    context = {'table_caption': table_caption, 'process_list': process_list}
    return render(request, 'lara_processes/process_list.html', context)

def searchForm(request):
    return render(request, 'lara_processes/search.html', context={} )

def search(request):
    search_string = request.POST['search']
    
    try:
        process_list = []
        curr_process = Process.objects.get(device__name = search_string)
        process_list.append( curr_process )
        
    except ValueError as err:
        sys.stderr.write("{}".format(err) )
        #~ return render(request, 'polls/detail.html', {
            #~ 'question': question,
            #~ 'error_message': "You didn't select a choice.",
        #~ })
    else:
        context = {'process_list': process_list}
        return render(request, 'lara_processes/results.html', context )
        #return HttpResponseRedirect(reverse('lara_processes:results', args=(lara_device_list,)))
        
def results(request, process_list):
    #question = get_object_or_404(Question, pk=question_id)
    context = {'process_list': process_list}
    return render(request, 'lara_processes/results.html', context )

def processDetails(request, process_id):
    curr_process = get_object_or_404(Device, pk=process_id)
    context = {'process': curr_process}
    return render(request, 'lara_processes/process.html', context)
