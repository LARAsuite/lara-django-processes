"""_____________________________________________________________________

:PROJECT: LARA

*lara_processes apps *

:details: lara_processes app configuration. This provides a genaric 
         django app configuration mechanism.
         For more details see:
         https://docs.djangoproject.com/en/2.0/ref/applications/

:file:    apps.py
:authors: mark doerr

:date: (creation)          20180627
:date: (last modification) 20180627

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.1"

from django.apps import AppConfig

class LaraProcessesConfig(AppConfig):
    name = 'lara_processes'
    # verbose_name = "enter a verbose name for your app: lara_processes here - this will be used in the admin interface"
    lara_app_icon = 'lara_processes_icon.svg'  # this will be used to display an icon, e.g. in the main LARA menu.

