"""_____________________________________________________________________

:PROJECT: LARA

*lara_processes admin *

:details: lara_processes admin module admin backend configuration.
         - 

:file:    admin.py
:authors: mark doerr

:date: (creation)          20180627
:date: (last modification) 20190706

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.4"

from django.contrib import admin

from .models import ItemCreatorType, ItemCreator, Method, Procedure, Process, \
                    MethodInstance, ProcedureInstance, ProcessInstance

class MethodAdmin(admin.ModelAdmin):
    pass
    #~ list_display = ('name','proc_class','description')
    #~ list_filter = ['proc_class']
    #~ search_fields = ['item_class']
    
class ProcessAdmin(admin.ModelAdmin):
    pass
    #~ list_display = ('name','proc_class','description')
    #~ list_filter = ['proc_class']
    #~ search_fields = ['item_class']
    
class ProcedureAdmin(admin.ModelAdmin):
    pass
    #~ list_display = ('name','proc_class','description')
    #~ list_filter = ['proc_class']
    #~ search_fields = ['name']

admin.site.register(ItemCreatorType)
admin.site.register(ItemCreator)

admin.site.register(Method, MethodAdmin)
admin.site.register(Procedure, ProcedureAdmin)
admin.site.register(Process, ProcessAdmin)

admin.site.register(MethodInstance)
admin.site.register(ProcedureInstance)
admin.site.register(ProcessInstance)

