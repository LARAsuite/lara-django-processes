"""_____________________________________________________________________

:PROJECT: LARA

*lara_processes models *

:details: lara_processes database models. 
         -

:file:    models.py
:authors: mark doerr

:date: (creation)          20180627
:date: (last modification) 20190705

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.4"

import logging

from django.db import models

from lara.models import DataType, MediaType, Tag, Namespace, ItemStatus
from lara_people.models import Entity
from lara_library.models import LibItem

from lara_substances.models import Substance, Polymer, MixtureComponent, Mixture
from lara_substances_store.models import SubstanceInstance, PolymerInstance, MixtureComponentInstance, MixtureInstance

from lara_material.models import Part, Device, Container
from lara_parts.models import PartInstance
from lara_devices.models import DeviceInstance
from lara_containers.models import ContainerInstance

class ExtraData(models.Model):
    """This class can be used to extend the process data, by extra information, 
       e.g.,   ... """
    extra_id =  models.AutoField(primary_key=True)
    data_type = models.ForeignKey(DataType, related_name="%(app_label)s_%(class)s_extra_data_related", 
                        related_query_name="%(app_label)s_%(class)s_extra_data", on_delete=models.CASCADE, blank=True, 
                        help_text="extra data type")
    extra_text = models.TextField(blank=True, null=True, help_text="generic text field") 
    extra_bin = models.BinaryField(blank=True, null=True, help_text="generic binary field")
    media_type = models.ForeignKey(MediaType, related_name="%(app_label)s_%(class)s_mediatype_related",
                                  on_delete=models.CASCADE, null=True, blank=True,  help_text="file type of extra data file")
    extra_file = models.FileField(upload_to='lara_processes/extra_data/', blank=True, null=True, help_text="rel. path/filename")

    class Meta:
        verbose_name_plural = 'ExtraData'

class ItemCreatorType(models.Model):
    """Type of Lib Item Creator: 
       e.g. author, editor, camera man,  
    """
    item_creator_type_id =  models.AutoField(primary_key=True)
    creator_type = models.TextField(unique=True, null=True, help_text="name of creator type")
    description = models.TextField(blank=True, null=True, help_text="description of extra data type")
    
    def __str__(self):
        return self.creator_type or ""
        
    def __repr__(self):
        return self.creator_type or ""

class ItemCreator(models.Model):
    """Lib Item Creator: 
       combination of type (author, editor, camera man) and Entity
    """
    item_creator_id =  models.AutoField(primary_key=True)
    entity = models.ForeignKey(Entity, related_name="entity_itemcreators",
                                  on_delete=models.CASCADE, null=True, blank=True, help_text="name of creator type")
    creator_type = models.ForeignKey(ItemCreatorType,  related_name="creator_type_creators",
                                  on_delete=models.CASCADE, null=True, blank=True, help_text="name of creator type")
    
    def __str__(self):
        return self.entity.name_full or ""
        
    def __repr__(self):
        return self.entity.name_full or ""

class MethodClass(models.Model):
    """ classes for methods Absorption ..."""
    method_class_id = models.AutoField(primary_key=True)
    method_class = models.TextField(unique=True, 
                   help_text="name of the method class, ")
    description = models.TextField(blank=True, help_text="description")

    def __str__(self):
        return self.method_class or ""
        
    class Meta:
        verbose_name_plural = "MethodClasses"
        #~ db_table = "lara_method_class"


class MethodAbstr(models.Model):
    """
         Abstract base class of all method-like classes (methods/procedures/processes)
    """
    namespace = models.ForeignKey(Namespace, related_name="%(app_label)s_%(class)s_namespace_related", 
                      related_query_name="%(app_label)s_%(class)s_namespace",
                      on_delete=models.CASCADE, null=True, blank=True, 
                      help_text="namespace of method/procedure/process")
    name = models.TextField(unique=True, blank=True, 
                            help_text="name of the method/procedure/process")
    
    # autogenerate this:
    name_full = models.TextField(unique=True, blank=True, 
                            help_text="fully qualified name of the method/procedure/process")
    proc_class = models.ForeignKey(MethodClass, on_delete=models.CASCADE, blank=True, 
                                   help_text="class of the method/procedure/process")
    URI = models.TextField(unique=True, blank=True, help_text="") # Unifor Resource Identifier
    version = models.TextField(blank=True, help_text="maj.min version of this method/procedure/process, e.g. 0.2")
    # -> JSON
    parameters = models.TextField(blank=True, help_text="method/procedure/process parameters")
    
    substances = models.ManyToManyField(Substance, related_name="%(app_label)s_%(class)s_substances_related", 
                            related_query_name="%(app_label)s_%(class)s_substances", blank=True, 
                            help_text=" e.g. for chemical synthesis,") # should be obsolete
    polymers = models.ManyToManyField(Polymer, related_name="%(app_label)s_%(class)s_polymers_related", 
                            related_query_name="%(app_label)s_%(class)s_polymers", blank=True, 
                            help_text="e.g. for sequencing ") #  should be obsolete
    mixtures_component = models.ManyToManyField(MixtureComponent, related_name="%(app_label)s_%(class)s_mixture_comp_related",
                            related_query_name="%(app_label)s_%(class)s_mixtures", blank=True, 
                            help_text="") # should be obsolete
    mixtures = models.ManyToManyField(Mixture, related_name="%(app_label)s_%(class)s_mixtures_related",
                            related_query_name="%(app_label)s_%(class)s_mixtures", blank=True, 
                            help_text="") # should be obsolete
    parts = models.ManyToManyField(Part, related_name="%(app_label)s_%(class)s_parts_related",
                            related_query_name="%(app_label)s_%(class)s_parts", blank=True, 
                            help_text="")
    containers = models.ManyToManyField(Container, related_name="%(app_label)s_%(class)s_containers_related",
                            related_query_name="%(app_label)s_%(class)s_containers", blank=True, 
                            help_text="")
    devices = models.ManyToManyField(Device, related_name="%(app_label)s_%(class)s_devices_related",
                            related_query_name="%(app_label)s_%(class)s_devices", blank=True, 
                            help_text="") 
    creators = models.ManyToManyField(ItemCreator, related_name="%(app_label)s_%(class)s_creators_related", 
                      related_query_name="%(app_label)s_%(class)s_item_creators", blank=True, 
                      help_text="creators of method/procedure/process")
    literature = models.ManyToManyField(LibItem, related_name="%(app_label)s_%(class)s_literature_related", 
                                        related_query_name="%(app_label)s_%(class)s_literature", blank=True, 
                                        help_text="literature relevant for this method, e.g. method articles")
    icon = models.TextField(blank=True, null=True, help_text="XML/SVG icon / symbol of the method")
    tags = models.ManyToManyField(Tag, related_name="%(app_label)s_%(class)s_tags_related", 
                                        related_query_name="%(app_label)s_%(class)s_tags",
                                        blank=True, help_text="tags")
    extra_data = models.ManyToManyField(ExtraData, related_name="%(app_label)s_%(class)s_extradata_related", 
                                        related_query_name="%(app_label)s_%(class)s_extradata", blank=True, 
                                        help_text="e.g. manual")
    description = models.TextField(blank=True, help_text="")
    remarks = models.TextField(blank=True, null=True, help_text="")
    
    def __str__(self):
        return self.name_full or ''
        
    def save(self, *args, **kwargs):
        #~ if not self.slug :
            #~ self.slug = '-'.join((self.proc_class, self.name, self.version)).lower()
        super().save(*args, **kwargs)
    
    class Meta:
        abstract = True

class Method(MethodAbstr):
    """ 
    Def. Method: A process by which a task is completed;
    a way of doing something (followed by the adposition of, to or for before the purpose of the process).
    E.g. method chromatography.HPLC , GC, NMR, PCR, ...
    This is a generic method representation, for a concrete Method in a particular lab, see MethodInstance
    """
    method_id = models.AutoField(primary_key=True)
    method = models.TextField(blank=True, help_text="method,e.g. chromatography.HPLC") 
    media_type = models.ForeignKey(MediaType, 
                                  on_delete=models.CASCADE, null=True, blank=True,  
                                  help_text="file type of method file")
    method_file = models.FileField(upload_to='lara_processes/methods/', blank=True, null=True, 
                                 help_text="rel. path/filename to methods") 


class Procedure(MethodAbstr):
    """
    A procedure is a (defined) sequence of actions leading to a certain result, 
    e.g. the sequence of gradient steps in an HPLC measurement, 
         a sequence of transfer steps on a liquid handler, an enzyme assay ...
    This is a generic Procedure representation, for a concrete Procedure in a particular lab, see MethodInstance
    """
    procedure_id = models.AutoField(primary_key=True)
    procedure = models.TextField(blank=True, help_text="the actual sequence of actions, best in python, JSON or XML syntax") 
    media_type = models.ForeignKey(MediaType, related_name="%(app_label)s_%(class)s_mediatype_related",
                                  on_delete=models.CASCADE, null=True, blank=True,  
                                  help_text="file type of method file")
    procedure_file = models.FileField(upload_to='lara_processes/procedures/', blank=True, null=True, 
                                 help_text="rel. path/filename to procedures") 

class Process(MethodAbstr):
    """
    A process is a combination of various procedures and decisions,
    e.g. a robot process, a fermentation process, ....
    """
    process_id = models.AutoField(primary_key=True)
    procedures = models.ManyToManyField(Procedure, related_name="procedures_processes", blank=True,
                                        help_text="all procedures of a process")


class MethodInstanceAbstr(models.Model):
    """
         Abstract base class of all method-like classes 
    """
    namespace = models.ForeignKey(Namespace, related_name='%(app_label)s_%(class)s_namespace_related', 
                      related_query_name="%(app_label)s_%(class)s_namespace",
                      on_delete=models.CASCADE, null=True, blank=True, 
                      help_text="namespace of method/procedure/process")
    name = models.TextField(unique=True, blank=True, 
                            help_text="name of the method/procedure/process")
    # autogenerate this:
    name_full = models.TextField(unique=True, blank=True, 
                            help_text="fully qualified name of the method/procedure/process")
    proc_class = models.ForeignKey(MethodClass, on_delete=models.CASCADE, blank=True, 
                                   help_text="class of the method ")
    URI = models.TextField(unique=True, blank=True, help_text="") # Unifor Resource Identifier
    version = models.TextField(blank=True, help_text="maj.min version of this method/procedure/process, e.g. 0.2")
    # -> JSON
    parameters = models.TextField(blank=True, help_text="method/procedure/process parameters")
    # execution date
    # duration
    state = models.ForeignKey(ItemStatus, related_name='%(app_label)s_%(class)s_namespace_related', 
                      related_query_name="%(app_label)s_%(class)s_namespace",
                      on_delete=models.CASCADE, null=True, blank=True, 
                      help_text="state of the method/procedure/process")
    substances = models.ManyToManyField(SubstanceInstance, related_name="%(app_label)s_%(class)s_substances_related", 
                            related_query_name="%(app_label)s_%(class)s_substances", blank=True, 
                            help_text=" e.g. for chemical synthesis,") # should be obsolete
    polymers = models.ManyToManyField(PolymerInstance, related_name="%(app_label)s_%(class)s_polymers_related", 
                            related_query_name="%(app_label)s_%(class)s_polymers", blank=True, 
                            help_text="e.g. for sequencing ") #  should be obsolete
    mixtures_component = models.ManyToManyField(MixtureComponentInstance, related_name="%(app_label)s_%(class)s_mixtures_related",
                            related_query_name="%(app_label)s_%(class)s_mixtures", blank=True, 
                            help_text="") # should be obsolete
    mixtures = models.ManyToManyField(MixtureInstance, related_name="%(app_label)s_%(class)s_mixtures_related",
                            related_query_name="%(app_label)s_%(class)s_mixtures", blank=True, 
                            help_text="") # should be obsolete
    parts = models.ManyToManyField(PartInstance, related_name="%(app_label)s_%(class)s_parts_related",
                            related_query_name="%(app_label)s_%(class)s_parts", blank=True, 
                            help_text="")
    containers = models.ManyToManyField(ContainerInstance, related_name="%(app_label)s_%(class)s_containers_related",
                            related_query_name="%(app_label)s_%(class)s_containers", blank=True, 
                            help_text="")
    devices = models.ManyToManyField(DeviceInstance, related_name="%(app_label)s_%(class)s_devices_related",
                            related_query_name="%(app_label)s_%(class)s_devices", blank=True, 
                            help_text="")
    creators = models.ManyToManyField(ItemCreator, related_name='%(app_label)s_%(class)s_item_creators_related', 
                      related_query_name="%(app_label)s_%(class)s_item_creators", blank=True, 
                      help_text="creators of method/procedure/process")
    literature = models.ManyToManyField(LibItem, related_name='%(app_label)s_%(class)s_literature_related', 
                                        related_query_name="%(app_label)s_%(class)s_literature", blank=True, 
                                        help_text="literature relevant for this method, e.g. method articles")
    icon = models.TextField(blank=True, null=True, help_text="XML/SVG icon / symbol of the method")
    tags = models.ManyToManyField(Tag, related_name="%(app_label)s_%(class)s_tags_related", 
                                        related_query_name="%(app_label)s_%(class)s_tags",
                                        blank=True, help_text="tags")
    extra_data = models.ManyToManyField(ExtraData, related_name="%(app_label)s_%(class)s_extradata_related", 
                                        related_query_name="%(app_label)s_%(class)s_extradata", blank=True, 
                                        help_text="e.g. manual")
    description = models.TextField(blank=True, help_text="")
    remarks = models.TextField(blank=True, null=True, help_text="")

    def __str__(self):
        return self.name_full or ''
        
    def save(self, *args, **kwargs):
        #~ if not self.slug :
            #~ self.slug = '-'.join((self.proc_class, self.name, self.version)).lower()
        super().save(*args, **kwargs)
    
    class Meta:
        abstract = True

class MethodInstance(MethodInstanceAbstr):
    """ 
    A process by which a task is completed;
    a way of doing something (followed by the adposition of, to or for before the purpose of the process).
    E.g. chromatography.HPLC, GC, NMR, PCR, ...
    This is concrete instance of a method
    """
    method_id = models.AutoField(primary_key=True)
    method_ref = models.ForeignKey(Method, related_name="method_method_instances",
                              on_delete=models.CASCADE, null=True, blank=True,  
                              help_text="reference method chromatography.HPLC")
    method = models.TextField(blank=True, help_text="method v1.2 chromatography.HPLC") 
    media_type = models.ForeignKey(MediaType, related_name="media_type_method_instances",
                                  on_delete=models.CASCADE, null=True, blank=True,  
                                  help_text="file type of method file")
    method_file = models.FileField(upload_to='lara_processes/methods/', blank=True, null=True, 
                                 help_text="rel. path/filename to methods") 

class ProcedureInstance(MethodInstanceAbstr):
    """
    A procedure is a (defined) sequence of actions leading to a certain result, 
    e.g. the sequence of gradient steps in an HPLC measurement, 
         a sequence of transfer steps on a liquid handler, an enzyme assay ...
    """
    procedure_id = models.AutoField(primary_key=True)
    procedure_ref = models.ForeignKey(Procedure, related_name="method_procedure_instances",
                                  on_delete=models.CASCADE, null=True, blank=True,  
                                  help_text="reference to abstract procedure")
    procedure = models.TextField(blank=True, help_text="the actual sequence of actions, best in python, JSON or XML syntax")                               
    media_type = models.ForeignKey(MediaType, related_name="edia_type_procedure_instances",
                                  on_delete=models.CASCADE, null=True, blank=True,  
                                  help_text="file type of method file")
    procedure_file = models.FileField(upload_to='lara_processes/procedures/', blank=True, null=True, 
                                 help_text="rel. path/filename to procedures") 

class ProcessInstance(MethodInstanceAbstr):
    """
    A process is a combination of various procedures and decisions,
    e.g. a robot process, a fermentation process, ....
    """
    process_id = models.AutoField(primary_key=True)
    process_ref = models.ForeignKey(Process, related_name="method_process_instances",
                                  on_delete=models.CASCADE, null=True, blank=True,  
                                  help_text="reference to abstract process")
    procedures = models.ManyToManyField(ProcedureInstance, related_name="procedures_processe_instances", blank=True,
                                        help_text="all procedures of a process")
