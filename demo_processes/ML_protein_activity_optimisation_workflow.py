"""
________________________________________________________________________

:PROJECT: LARA

*ML_optimisation.py - machine learning protein activity optimisation pipeline *

:details: 
         - 
:file:    parameter_optimisation.py
:authors:  Stefan Born (stefan.born@tu-berlin.de)
           mark doerr  (mark@uni-greifswald.de)

:date: (creation)          20190626
:date: (last modification) 20190626

.. note:: -
.. todo:: - #    

________________________________________________________________________
"""
__version__ = "0.0.1"


# protein activity optimisation pipeline

propose_initial_mutations()

measure_protein_activities()

learn_sequence_activity_relations()
  optimise_parameters()
  build_activity_model()
  

propose_new_mutations()

