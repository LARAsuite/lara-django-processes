.. lara_processes documentation master file, created by
   sphinx-quickstart on Sat Mar 24 15:13:29 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to *lara_processes*'s documentation!
====================================================

*lara_processes* can be used to quickly extend the LARAsuite.

Contents:

.. toctree::
   :maxdepth: 2

   readme
   build_package
   modules
   todo
   changelog
   

Project Version
----------------

.. include:: ../VERSION


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

